import 'package:DemoGabriel/features/details/profile_page.dart';
import 'package:DemoGabriel/model/prisoners_model.dart';
import 'package:DemoGabriel/service/prisoner_api.dart';
import 'package:DemoGabriel/utils/nav.dart';
import 'package:DemoGabriel/widgets/text_error.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class PrisonersListView extends StatefulWidget {
  @override
  _PrisonersListViewState createState() => _PrisonersListViewState();
}

class _PrisonersListViewState extends State<PrisonersListView> with AutomaticKeepAliveClientMixin<PrisonersListView> {

  @override
  bool get wantKeepAlive => true;

  List<Arrest> arrests;

  final _model = PrisonersModel();

  @override
  void initState(){
    super.initState();

    _fetch();
  }

  void _fetch(){
    _model.fetch();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Observer(
      builder: (_) {
        List<Arrest> arrests = _model.arrests;

        if (_model.error != null){
          return TextError("Não foi possível buscar qualquer dado\n\nClique aqui para tentar novamente",
            onPressed: _fetch,
          );
        }

        if(arrests == null){
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        return _listView(arrests);
      },
    );
  }
  _listView(List<Arrest> arrests){
    return Container(
      padding: EdgeInsets.all(15),
      child: ListView.builder(
        itemCount: arrests.length,
        itemBuilder: (context, index) {
          Arrest a = arrests[index];
          return Card(
            color: Colors.grey[100],
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: Image.network(
                      a.mugshotUrl,
                      width: 100,
                    ),
                  ),
                  Text(
                    a.name,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 25),
                  ),
                  Text(
                    "Acusações: ${a.charges}",
                    style: TextStyle(fontSize: 16),
                  ),
                  ButtonBarTheme(
                    data: ButtonBarThemeData(),
                    child: ButtonBar(
                      children: <Widget>[
                        // ignore: deprecated_member_use
                        FlatButton(
                          child: const Text('DETALHES'),
                          onPressed: () => _onClickDetalhes(a.charges, a.name, a.mugshotUrl, a.id, a.dateFormatted),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
  _onClickDetalhes(List<dynamic> charges, String name, String mugshotUrl, int id, String dateFormatted) {
    push(context, ProfilePage(charges, name, dateFormatted, mugshotUrl, id));
  }
}
