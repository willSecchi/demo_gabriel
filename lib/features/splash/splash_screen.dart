import 'package:DemoGabriel/features/main/main_screen.dart';
import 'package:DemoGabriel/utils/nav.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 2)).then((value) => _navigate());
  }

  @override
  Widget build(BuildContext context) {
  Size size = MediaQuery.of(context).size;
    return Container(
      color: Colors.cyan,
      height: size.height,
      width: size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset("assets/flutter.png", 
          width: size.width * 0.5,
          )
        ],
      ),
    );
  }

  _navigate(){
    push(context, MainScreen());
  }
}
