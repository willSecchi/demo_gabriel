import 'package:DemoGabriel/features/details/appbar_widget.dart';
import 'package:DemoGabriel/features/details/button_widget.dart';
import 'package:DemoGabriel/features/details/profile_widget.dart';
import 'package:DemoGabriel/service/api_salvar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  final List<dynamic> charges;
  final String name;
  final String mugshotUrl;
  final int id;
  final String dateFormatted;

  ProfilePage(this.charges, this.name, this.dateFormatted, this.mugshotUrl, this.id);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: buildAppBar(context),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          ProfileWidget(
            imagePath: widget.mugshotUrl,
            onClicked: () async {},
          ),
          const SizedBox(height: 24),
          buildName(),
          const SizedBox(height: 24),
          Center(child: buildSave()),
          const SizedBox(height: 48),
          buildAbout(),
        ],
      ),
    );
  }

  Widget buildName() => Column(
        children: [
          Text(
            widget.name,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(height: 4),
          Text(
            "${widget.id}",
            style: TextStyle(color: Colors.grey),
          )
        ],
      );

  Widget buildSave() => ButtonWidget(
        text: 'Salvar',
        onClicked: () {
          Map _body = {
            'info1': 'info1',
            'info2': 'info2',
            'info3': 'info3'
          };
          try {
            var response = ApiSalvar.internal().post(query: '/savePrisonerProfile', body: _body);
          } catch (e) {
            print(e);
          }
        },
      );

  Widget buildAbout() => Container(
        padding: EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Acusações',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 16),
            Text(
              "${widget.charges}",
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
            const SizedBox(height: 16),
            Text(
              "${widget.dateFormatted}",
              style: TextStyle(fontSize: 16, height: 1.4),
            ),
          ],
        ),
      );
}
