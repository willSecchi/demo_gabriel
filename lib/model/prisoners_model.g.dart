// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'prisoners_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PrisonersModel on PrisonersModelBase, Store {
  final _$arrestsAtom = Atom(name: 'PrisonersModelBase.arrests');

  @override
  List<Arrest> get arrests {
    _$arrestsAtom.reportRead();
    return super.arrests;
  }

  @override
  set arrests(List<Arrest> value) {
    _$arrestsAtom.reportWrite(value, super.arrests, () {
      super.arrests = value;
    });
  }

  final _$errorAtom = Atom(name: 'PrisonersModelBase.error');

  @override
  Exception get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(Exception value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  final _$fetchAsyncAction = AsyncAction('PrisonersModelBase.fetch');

  @override
  Future fetch() {
    return _$fetchAsyncAction.run(() => super.fetch());
  }

  @override
  String toString() {
    return '''
arrests: ${arrests},
error: ${error}
    ''';
  }
}
