import 'package:DemoGabriel/service/prisoner_api.dart';
import 'package:mobx/mobx.dart';

part 'prisoners_model.g.dart';

class PrisonersModel = PrisonersModelBase with _$PrisonersModel;

abstract class PrisonersModelBase with Store {

  @observable
  List<Arrest> arrests;

  @observable
  Exception error;

  @action
  fetch() async {
    try {
      error = null;

      this.arrests = await JailBase.getRecentArrests();

    } catch (e) {
      print(e);
      error = e;
    }
  }
}