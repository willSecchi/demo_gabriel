import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http/io_client.dart';

class ApiSalvar {
  static final ApiSalvar _instance = ApiSalvar.internal();

  factory ApiSalvar() => _instance;

  ApiSalvar.internal();

  String authorization;

  String url = "https://..........";

  Future<Map<String, dynamic>> get({
    @required String query,
    Duration duration = const Duration(seconds: 10),
    String token,
  }) async {
    try {
      String returnValue;
      String currentUrl = this.url + query;
      print(currentUrl);

      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final http = new IOClient(ioc);
      await Future.delayed(Duration(milliseconds: 100));

      await http
          .get(
        Uri.parse(currentUrl),
        headers: {
          "Authorization": authorization,
          "Content-Type": "application/json",
          "Accept": "application/json",
          if (token != null) 'token': token,
        },
      )
          .timeout(duration)
          .then(
            (Response response) {
          returnValue = utf8.decode(response.bodyBytes);
        },
      );

      if (returnValue.startsWith('[')) {
        returnValue = "{\"data\": $returnValue}";
      }

      return jsonDecode(returnValue);
    } on TimeoutException catch (e) {
      return {'connection': e.toString()};
    } catch (e) {
      return {'error': e.toString()};
    }
  }

  Future<Map<String, dynamic>> post({
    @required String query,
    @required Map<String, dynamic> body,
    Duration duration = const Duration(seconds: 10),
  }) async {
    try {
      String returnValue;
      String currentUrl = this.url + query;
      print(currentUrl);
      print(body);
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final http = new IOClient(ioc);

      await http
          .post(
        Uri.parse(currentUrl),
        headers: {
          "Authorization": authorization,
          "Content-Type": "application/json",
          "Accept": "application/json",
          'app_token': '',
          'access_token': '',
        },
        body: jsonEncode(body ?? {}),
      )
          .timeout(duration)
          .then(
            (Response response) {
          returnValue = utf8.decode(response.bodyBytes);
        },
      );

      if (returnValue.startsWith('[')) {
        returnValue = "{\"data\": $returnValue}";
      }

      print(returnValue);

      return jsonDecode(returnValue);
    } on TimeoutException catch (e) {
      return {'connection': e.toString()};
    } catch (e) {
      return {'error': e.toString()};
    }
  }

  Future<Map<String, dynamic>> put({
    @required String query,
    @required Map<String, dynamic> body,
    Duration duration = const Duration(seconds: 10),
  }) async {
    try {
      String returnValue;
      String currentUrl = this.url + query;
      print(currentUrl);
      print(body);
      await Future.delayed(Duration(milliseconds: 100));
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final http = new IOClient(ioc);
      await Future.delayed(Duration(milliseconds: 100));
      await http
          .put(
        Uri.parse(currentUrl),
        headers: {
          "Authorization": authorization,
          "Content-Type": "application/json",
          "Accept": "application/json",
          'app_token': '',
          'access_token': '',
        },
        body: jsonEncode(body),
      )
          .timeout(duration)
          .then(
            (Response response) {
          returnValue = utf8.decode(response.bodyBytes);
        },
      );

      if (returnValue.startsWith('[')) {
        returnValue = "{\"data\": $returnValue}";
      }
      return jsonDecode(returnValue);
    } on TimeoutException catch (e) {
      return {'connection': e.toString()};
    } catch (e) {
      return {'error': e.toString()};
    }
  }

  Future<Map<String, dynamic>> delete({
    @required String query,
    @required Map<String, dynamic> body,
    Duration duration = const Duration(seconds: 10),
  }) async {
    try {
      String returnValue;
      String currentUrl = this.url + query;
      print(currentUrl);
      await Future.delayed(Duration(milliseconds: 100));
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final http = new IOClient(ioc);
      await Future.delayed(Duration(milliseconds: 100));
      await http
          .delete(
        Uri.parse(currentUrl),
        headers: {
          "Authorization": authorization,
          "Content-Type": "application/json",
          "Accept": "application/json",
          'app_token': '',
          'access_token': '',
        },
      )
          .timeout(duration)
          .then(
            (Response response) {
          returnValue = utf8.decode(response.bodyBytes);
        },
      );

      if (returnValue.startsWith('[')) {
        returnValue = "{\"data\": $returnValue}";
      }

      return jsonDecode(returnValue);
    } on TimeoutException catch (e) {
      return {'connection': e.toString()};
    } catch (e) {
      return {'error': e.toString()};
    }
  }

  Future<bool> testConnection() async {
    Map<String, dynamic> json = await get(query: "/controller/ping", duration: Duration(seconds: 5));
    if (json.containsKey('connection') || json.containsKey('error')) return false;
    return true;
  }

  Future<void> getAuthorizationCredetials() async {
    Map<String, dynamic> json = await () async {
      try {
        String returnValue;
        String currentUrl = this.url + '/oauth/token';
        print(currentUrl);
        await Future.delayed(Duration(milliseconds: 100));

        String username = '';
        String password = '';

        print(username);
        print(password);

        Map<String, dynamic> body = {
          "grant_type": '',
          "username": '',
          "password": '',
        };

        print(body);
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final http = new IOClient(ioc);
        await Future.delayed(Duration(milliseconds: 100));

        await http
            .post(
          Uri.parse(currentUrl),
          headers: {
            "Authorization": "",
            "Content-Type": "application/x-www-form-urlencoded"
          },
          body: body,
        )
            .timeout(Duration(seconds: 5))
            .then(
              (Response response) {
            returnValue = utf8.decode(response.bodyBytes);
          },
        );

        return jsonDecode(returnValue);
      } on TimeoutException catch (e) {
        return {'connection': e.toString()};
      } catch (e) {
        return {'error': e.toString()};
      }
    }();
    print(json);
    if (json.containsKey('connection') || json.containsKey('error')) return;
    authorization = json['token_type'] + ' ' + json['access_token'];
  }
}